 <!DOCTYPE html>
<html>
	<head>
		<title>Word Study Analysis</title>
		<style>
			header, footer 
			{
				padding: 1em;
				color: white;
				background-color: #06452F;
				text-align: center;
			}
			aside
			{
				float: left;
				max-width: 160px;
				margin: 0;
				padding: 1em;
				background-color: #5F5F5F;
				color: white;
			}
			body
			{
				margin: 0;
				padding: 1em;
			}
		</style>
	</head>
	<body>

		<header>
			<h1>Castleton University Word Study</h1>
		</header>
		<aside>
			Home<br>
			Analysis<br>
			Results<br>
		</aside>
		<p align="Center">Choose the information you want to analyze.</p>

	</body>
</html> 